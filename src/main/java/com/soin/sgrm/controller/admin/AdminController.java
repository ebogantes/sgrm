package com.soin.sgrm.controller.admin;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.soin.sgrm.exception.Sentry;
import com.soin.sgrm.model.Priority;
import com.soin.sgrm.model.Tree;
import com.soin.sgrm.service.PriorityService;
import com.soin.sgrm.service.TreeService;
import com.soin.sgrm.utils.JsonResponse;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	TreeService treeService;

	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String index(HttpServletRequest request, Locale locale, Model model, HttpSession session) {
		return "/admin/home";
	}

}
